package com.sysomos.grid.crawler.request;

import java.util.ArrayList;
import java.util.List;

/**
* @author ccai on 2016-02-03
*/
public class SummaryRequest {

	private List<Url> urls;

	
    public List<Url> getUrls() {
    	return urls;
    }
	
    public void setUrls(ArrayList<Url> urls) {
    	this.urls = urls;
    }
	
	
}
