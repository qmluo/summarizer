package com.sysomos.grid.crawler;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.context.web.SpringBootServletInitializer;
import org.springframework.boot.legacy.context.web.MetricFilterAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

import javax.servlet.Filter;

@Configuration
@ComponentScan
@EnableAutoConfiguration
public class CrawlerApplication extends SpringBootServletInitializer{

	public static void main(String[] args) {
		SpringApplication.run(CrawlerApplication.class, args);
	}
   
	@Bean
	public Filter metricFilter(){
		MetricFilterAutoConfiguration conf = new MetricFilterAutoConfiguration();		
		return conf.metricFilter();
	}
	
	@Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(CrawlerApplication.class);
    }

}
