package com.sysomos.grid.crawler.rest.impl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestBody;

import com.sysomos.grid.crawler.request.SummaryRequest;
import com.sysomos.grid.crawler.request.Url;
import com.sysomos.grid.crawler.response.SummaryResponse;
import com.sysomos.grid.crawler.rest.ICrawlerSearch;


/**
 * @author ccai on 2016-02-03
 */
@Component
public class CrawlerSearch implements ICrawlerSearch {

	private static final Logger LOG = LoggerFactory.getLogger(CrawlerSearch.class);

	@Value("${desk.found}")
	private int found;
		
	@Override
	public SummaryResponse getSummaries(@RequestBody SummaryRequest request){
		
		SummaryResponse response = new SummaryResponse();
		response.setSummary(request.getUrls().get(0).getUrl());
		response.setStatusCode(found);
		return response;
	}
	
	@Override
	public SummaryResponse getSummaryForUrl(@RequestBody List<Url> request){
		
		SummaryResponse response = new SummaryResponse();
		response.setSummary(request.get(0).getUrl());
		response.setStatusCode(found);
		return response;
	}	
	@Override	
	public SummaryResponse getSummary(@RequestBody Url request){

		SummaryResponse response = new SummaryResponse();
		response.setSummary(request.getUrl());
		response.setStatusCode(found);
		return response;
	}
}
