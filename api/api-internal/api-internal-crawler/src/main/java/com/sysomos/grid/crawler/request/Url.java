package com.sysomos.grid.crawler.request;

/**
* @author ccai on 2016-02-03
*/

public class Url {

	private String url;
	
    public String getUrl() {
    	return url;
    }
	
    public void setUrl(String url) {
    	this.url = url;
    }
	
	
}
