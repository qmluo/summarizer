package com.sysomos.grid.crawler.rest.impl;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestBody;

import com.sysomos.grid.crawler.processor.CrawlerFetcher;
import com.sysomos.grid.crawler.request.SummaryRequest;
import com.sysomos.grid.crawler.request.Url;
import com.sysomos.grid.crawler.response.SummaryResponse;
import com.sysomos.grid.crawler.rest.ICrawlerSearch;


/**
 * @author ccai on 2016-02-03
 */
@Component
public class CrawlerSearch implements ICrawlerSearch {

	private static final Logger LOG = LoggerFactory.getLogger(CrawlerSearch.class);
    ExecutorService executor = Executors.newFixedThreadPool(50);

	@Value("${desk.found}")
	private int found;
		
	@Override
	public SummaryResponse getSummaries(@RequestBody SummaryRequest request){
		
		SummaryResponse response = new SummaryResponse();
		List<String> urls = new ArrayList<String>();
		List<Url> list = request.getUrls();
		String text = "";
		if(list != null && !list.isEmpty()){
			for(Url item: list ) {
				urls.add(item.getUrl());
			}
			text = getSummaryText(urls);
		}
//		response.setSummary(request.getUrls().get(0).getUrl());
		response.setSummary(text);
		response.setStatusCode(found);
		return response;
	}
	
	@Override
	public SummaryResponse getSummaryForUrl(@RequestBody List<Url> request){
		
		SummaryResponse response = new SummaryResponse();
		List<String> urls = new ArrayList<String>();
		String text = "";
		if(request != null && !request.isEmpty()){
			for(Url item: request ) {
				urls.add(item.getUrl());
			}
			text = getSummaryText(urls);
		}
//		response.setSummary(request.getUrls().get(0).getUrl());
		response.setSummary(text);
		
		response.setStatusCode(found);
		return response;
	}	
	@Override	
	public SummaryResponse getSummary(@RequestBody Url request){

		SummaryResponse response = new SummaryResponse();
		List<String> urls = new ArrayList<String>();
		String text = "";
		if(request != null && !request.getUrl().isEmpty()){
			urls.add(request.getUrl());
			text = getSummaryText(urls);
		}		
		response.setSummary(text);
//		response.setSummary(request.getUrl());
		response.setStatusCode(found);
		return response;
	}
	
	private String getSummaryText(List<String> urls) {
		Set<Future<String>> set = new HashSet<Future<String>>();
		for (String url : urls) {
			Callable<String> callable = new CrawlerFetcher(url);
			Future<String> future = executor.submit(callable);
			set.add(future);
		}
		StringBuilder buf = new StringBuilder();
		for (Future<String> future : set) {
			try {
				buf.append(future.get());
			} catch (InterruptedException e) {
				e.printStackTrace();
			} catch (ExecutionException e) {
				e.printStackTrace();
			}
		}
		
		return buf.toString();
	}
}
