package com.sysomos.grid.crawler.response;


/**
 * @author ccai on 2016-02-03
 */

public class SummaryResponse {
	
	private Integer statusCode;
	private String summary;


	public Integer getStatusCode() {
		return statusCode;
	}

	public void setStatusCode(Integer statusCode) {
		this.statusCode = statusCode;
	}

	
    public String getSummary() {
    	return summary;
    }

	
    public void setSummary(String summary) {
    	this.summary = summary;
    }



}
