package com.sysomos.grid.crawler.common;

public class CrawlerQuery {
	private String url;
	private String summary;
	private int status;
	
	public CrawlerQuery(String urlstring){
		this.url = urlstring;
	}
	
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public String getSummary() {
		return summary;
	}
	public void setSummary(String summary) {
		this.summary = summary;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
}
