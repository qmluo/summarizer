package com.sysomos.grid.crawler.processor;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.FutureTask;
import java.util.concurrent.LinkedBlockingDeque;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.zip.GZIPInputStream;
import java.util.zip.Inflater;
import java.util.zip.InflaterInputStream;

import org.apache.log4j.Logger;
import org.apache.tika.exception.TikaException;
import org.apache.tika.metadata.Metadata;
import org.apache.tika.parser.ParseContext;
import org.apache.tika.parser.html.HtmlParser;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;
import org.xml.sax.SAXException;

import com.sysomos.grid.crawler.common.SummarizerUtil;

public class CrawlerFetcher implements Callable<String> {
	static {
		System.setProperty("appLogName.log", "FetcherApp.log");
	}
	static final Logger logger = Logger.getLogger(CrawlerFetcher.class);

	public static final String HTTP_USER_AGENT = "Mozilla/5.0 "
			+ "(compatible; Sysomos/1.0; +http://www.sysomos.com/; Sysomos)";

	public static final int CONNECT_TIMEOUT = 180 * 1000; // 3 mins
	public static final int READ_TIMEOUT = 180 * 1000; // 3 mins
	public static final int CRAWL_INTERVAL = 500; // .5 sec
	private static final long MAX_PAGE_SIZE = 1024 * 1024 * 64;
	// private static final String outPath = "/tmp/";

	private String url;
	private String userAgentString;
	private final HtmlParser htmlParser;
	private final ParseContext parseContext;
	private BlockingQueue<String> queue = new LinkedBlockingQueue<String>();
	private String result;

	public CrawlerFetcher(String url /*BlockingQueue<String> q*/) {
		this.setUrl(url);
		htmlParser = new HtmlParser();
		parseContext = new ParseContext();
//		this.queue = q;
	}


	public URL getURL(String urlString) throws MalformedURLException {
		urlString = urlString.replaceAll(" ", "%20");

		URL result = new URL(urlString);
		// As such java is supposed to figure out that
		// url="http:www.blogscope.net" is malformed, but unfortunately it
		// doesnt do that. Thats why we have the next if condition to check that
		// ourselves.
		if (!urlString.contains("://")) {
			throw new MalformedURLException("Malformed URL " + urlString + " as it is supposed to contain '://'");
		}
		return result;
	}

	public String getBodyText() throws IOException {
		String urlString = getUrl();
		String retText = "";
		// long startTime = System.currentTimeMillis();
		InputStream resultingInputStream = null;
		HttpURLConnection connection;
		URL url = getURL(urlString);
		connection = (HttpURLConnection) url.openConnection();
		connection.setConnectTimeout(CONNECT_TIMEOUT);
		connection.setReadTimeout(READ_TIMEOUT);
		// allow both GZip and Deflate (ZLib) encodings
		connection.setRequestProperty("Accept-Encoding", "gzip, deflate");
		connection.setRequestProperty("User-Agent", getUserAgentString());
		String encoding = connection.getContentEncoding();
		int responseCode = connection.getResponseCode();

		if (responseCode == HttpURLConnection.HTTP_OK) {
			logger.info("Downloading page..." + urlString);

			if (encoding != null && encoding.equalsIgnoreCase("gzip")) {
				resultingInputStream = new GZIPInputStream(connection.getInputStream());
			} else if (encoding != null && encoding.equalsIgnoreCase("deflate")) {
				resultingInputStream = new InflaterInputStream(connection.getInputStream(), new Inflater(true));
			} else {
				resultingInputStream = connection.getInputStream();
			}
		}

		// html parsing
		Metadata metadata = new Metadata();
		HtmlContentHandler contentHandler = new HtmlContentHandler();
		// try (InputStream inputStream = new ByteArrayInputStream(page.getContentData())) {
		// if (resultingInputStream != null) {
		try {
			htmlParser.parse(connection.getInputStream(), contentHandler, metadata, parseContext);
			retText = contentHandler.getBodyText().trim();
		} catch (SAXException | TikaException e) {
			logger.error(e.getMessage());
		}
		// }

		return retText;
	}

	public String getString() throws MalformedURLException, IOException {
		String urlString = getUrl();
		String value = "";
//		try {
			URLConnection connection = new URL(urlString).openConnection();
			String html = "";
			BufferedReader in = null;
			connection.setReadTimeout(10000);
			in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
			String inputLine;
			while ((inputLine = in.readLine()) != null) {
				html += inputLine + "\n";
			}
			in.close();
			System.out.println(html);
//		} catch (IOException ex) {
//			Logger.getLogger(CrawlerFetcher.class.getName()).log(Level.ERROR, null, ex);
//		}
		return value;
	}
	
	public String getHTMLBodyText() throws IOException {
		String contents;
		String urlString = getUrl();
		Document doc = Jsoup.connect(urlString ).get();
		Elements body = doc.select("body");
		contents = body.text();
		System.out.println("Jsoup: contents -> " + contents);
		return contents;
	}

	public String getUserAgentString() {
		if (userAgentString != null) {
			return userAgentString;
		}
		return HTTP_USER_AGENT;
	}

//	@Override
//	public void run() {
//		try {
////			String text = getString();
//			String text = getHTMLBodyText();
//			queue.offer(text);
//		} catch (IOException e) {
//			logger.error(e.getMessage());
//		}
//	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	@Override
	public String call() throws Exception {
		return SummarizerUtil.getSummary(getHTMLBodyText());
	}

	public static void main(String[] args) {
		// String url = "http://artyears.wordpress.com/";
		String url = "http://www.mtv.com/news/";
		BlockingQueue q = new LinkedBlockingDeque<>();
//		CrawlerFetcher cf = new CrawlerFetcher(url, q);
		CrawlerFetcher cf = new CrawlerFetcher(url);
		FutureTask<String> futureTask1 = new FutureTask(cf);
//		Thread t = new Thread(cf);
//		t.start();
        ExecutorService executor = Executors.newFixedThreadPool(2);
        executor.submit(futureTask1);
        boolean running = true;
        while(running) {
        	if(futureTask1.isDone() ){
               	try {
    				String s = futureTask1.get();
    				System.out.println("Content text = " + s);
    				running = false;
    			} catch (InterruptedException e) {
    				running = false;
    				futureTask1.cancel(true);
    				e.printStackTrace();
    			} catch (ExecutionException e) {
    				running = false;
    				futureTask1.cancel(true);
    				e.printStackTrace();
    			}
        	}
         }// while
    	executor.shutdown();
	}
}
