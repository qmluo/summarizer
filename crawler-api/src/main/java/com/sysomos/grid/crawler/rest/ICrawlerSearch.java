package com.sysomos.grid.crawler.rest;

import java.util.List;

import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.sysomos.grid.crawler.request.SummaryRequest;
import com.sysomos.grid.crawler.request.Url;
import com.sysomos.grid.crawler.response.SummaryResponse;


/**
 * @author ccai on 2016-02-03
 */
@RestController
@RequestMapping(value = "api/v1/crawler", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
public interface ICrawlerSearch {
	
	@RequestMapping(value="/summary", method = RequestMethod.POST)
	SummaryResponse getSummaries(@RequestBody SummaryRequest request);
	
	@RequestMapping(value="/summary/urls", method = RequestMethod.POST)
	SummaryResponse getSummaryForUrl(@RequestBody List<Url> request);	
	
	@RequestMapping(value="/summary/1", method = RequestMethod.POST)
	SummaryResponse getSummary(@RequestBody Url request);
}
