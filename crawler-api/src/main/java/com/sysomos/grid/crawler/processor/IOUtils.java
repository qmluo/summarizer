package com.sysomos.grid.crawler.processor;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Reader;
import java.io.StringReader;
import java.io.StringWriter;
import java.io.Writer;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.compress.compressors.bzip2.BZip2CompressorOutputStream;
import org.apache.log4j.Logger;

/**
 * I/O related utility functions. All I/O takes places with default charset
 * (UTF-8) as specified in the BaseFunctions.
 */
public class IOUtils {

    static Logger logger = Logger.getLogger(IOUtils.class);

    public static final long ONE_KB = 1024;

    public static final long ONE_MB = 1024 * 1024;

    public static final long ONE_GB = 1024L * 1024L * 1024L;
    /**
     * UTF8 charset, the application-wide default.
     */
    public static final Charset UTF8 = Charset.forName("UTF-8");

    /**
     * BZ2 compression of the original file and saves to destFilexPath.
     */
    public static void compressBZ2(String originalFilePath, String destFilePath)
            throws IOException {
        int bufferSize = 1024 * 1000;
        try {
            BZip2CompressorOutputStream os = new BZip2CompressorOutputStream(
                    new FileOutputStream(new File(destFilePath)));
            FileInputStream is = new FileInputStream(originalFilePath);
            final byte[] buffer = new byte[bufferSize];
            int n = 0;
            while (-1 != (n = is.read(buffer))) {
                os.write(buffer, 0, n);
            }
            os.close();
            is.close();
        } catch (IOException e) {
            logger.error("[IOUtils.compressBZ2] " + "Failed to compress "
                    + originalFilePath + " to " + destFilePath, e);
            throw e;
        }

    }

    /**
     * Copy bytes from <code>InputStream</code> to an <code>OutputStream</code>.
     * This method buffers the input internally, so there is no need to use a
     * <code>BufferedInputStream</code>.
     * <p>
     * This code is copied from Apache Commons IO utils code
     *
     * @param input the <code>InputStream</code> to read from
     * @param output the <code>OutputStream</code> to write to
     * @return the number of bytes copied
     * @throws NullPointerException if the input or output is null
     * @throws IOException if an I/O error occurs
     */
    public static long copy(InputStream input, OutputStream output)
            throws IOException {
        int bufferSize = 4 * 1024;
        byte[] buffer = new byte[bufferSize];
        long count = 0;
        int n = 0;
        while (-1 != (n = input.read(buffer))) {
            output.write(buffer, 0, n);
            count += n;
        }
        return count;
    }

    /**
     * Counts the number of files and directories in a dir recursively. If a
     * filename is passed as an argument, zero is returned. If the directory is
     * empty, 1 is returned (i.e., the directory itself is counted).
     */
    public static int countFiles(File f) {
        if (f.isFile()) {
            return 0;
        } else if (f.isDirectory()) {
            int count = 1;
            File[] chidren = f.listFiles();
            for (File c : chidren) {
                if (c.isFile()) {
                    count++;
                } else {
                    count += countFiles(c);
                }
            }
            return count;
        } else {
            logger.error("[IOUtils.countFiles] " + f
                    + " is neither a file nor a directory");
            return -1;
        }
    }

    /**
     * counts the number of lines in the file.
     *
     * @throws IOException in case of an error
     */
    public static int countLines(String filename) throws IOException {
        BufferedReader reader = getBR(filename);
        int count = 0;
        String line = reader.readLine();
        while (line != null) {
            count++;
            line = reader.readLine();
        }
        reader.close();
        return count;
    }

    /**
     * create a directory if it does not already exist. if required, parent
     * directories are also automatically created.
     */
    public static void createDir(String dir) {
        File dirFile = new File(dir);
        if (!dirFile.exists()) {
            dirFile.mkdirs();
        }
    }

    /**
     * Create all parent directories required for creating this file on the
     * local filesystem. WARNING: this method deletes the folder in
     * <code>filename</code> if it exists
     */
    public static void createDirsForFile(String filename) {
        File file = new File(filename);
        if (!file.exists()) {
            file.mkdirs();
            delete(filename);
        }
    }

    /**
     * Creates a temp directory with given prefix.
     */
    public static File createTempDir(String prefix) throws IOException {
        File tempFile = File.createTempFile(prefix, "");
        if (!tempFile.delete()) {
            throw new IOException();
        }
        if (!tempFile.mkdir()) {
            throw new IOException();
        }
        return tempFile;
    }

    /**
     * delete a file or a directory recursively.
     *
     * @param dir file or directory name to delete
     * @return ture if and only if successfully deleted
     */
    public static boolean delete(File dir) {
        if (dir.isDirectory()) {
            String[] children = dir.list();
            for (int i = 0; i < children.length; i++) {
                boolean success = delete(new File(dir, children[i]));
                if (!success) {
                    return false;
                }
            }
        }
        return dir.delete();
    }

    /**
     * delete a file or a directory recursively.
     *
     * @param dir file or directory name to delete
     * @return ture if and only if successfully deleted
     */
    public static boolean delete(String dir) {
        return delete(new File(dir));
    }

    /**
     * Returns disk size of a dir in bytes
     */
    public static long dirSize(File dir) {
        long size = 0;
        if (dir.isFile()) {
            size = dir.length();
        } else {
            File[] subFiles = dir.listFiles();

            for (File file : subFiles) {
                if (file.isFile()) {
                    size += file.length();
                } else {
                    size += dirSize(file);
                }

            }
        }
        return size;
    }

    public static long dirSize(String dirPath) {
        return dirSize(new File(dirPath));
    }

    /**
     * test to see if a given path to a directory or a file exists or not
     *
     * @param path the target path to check
     * @return whether or not path exists
     */
    public static boolean exists(String path) {
        File f = new File(path);
        return f.exists();
    }

    /**
     * Shortcut for getting buffered reader. Uses UTF-8 encoding. 8MB buffer
     * performs better statistically than 64KB buffer.
     */
    public static BufferedReader getBR(String filename) throws IOException {
        return new BufferedReader(new InputStreamReader(new FileInputStream(
                filename), UTF8), 1024 * 1024 * 8);
    }

    /**
     * Shortcut for getting buffered writer. Uses UTF-8 encoding.
     */
    public static BufferedWriter getBW(String filename) throws IOException {
        return new BufferedWriter(new OutputStreamWriter(new FileOutputStream(
                filename), UTF8), 1024 * 1024 * 8);
    }

    /**
     * get the contents of input stream as a byte array.
     */
    public static byte[] getByteContents(InputStream is) throws IOException {
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        copy(is, bos);
        return bos.toByteArray();
    }

    /**
     * reads all contents from the provided file as an array of byte (suitable
     * for binary files).
     *
     * @param filename name of file to read from
     */
    public static byte[] getBytes(String filename) throws IOException {
        File file = new File(filename);
        long length = file.length();
        if (length > Integer.MAX_VALUE) {
            throw new IOException("File is too large to process: " + filename
                    + " with " + length + " bytes");
        }

        BufferedInputStream bis = new BufferedInputStream(new FileInputStream(
                filename));
        byte[] bytes = new byte[(int) length];
        int offset = 0;
        int numRead = 0;
        while ((offset < bytes.length)) {
            numRead = bis.read(bytes, offset, bytes.length - offset);
            if (numRead < 0) {
                break;
            }
            offset += numRead;
        }
        bis.close();
        if (offset < bytes.length) {
            throw new IOException("Could not completely read file "
                    + file.getName());
        }
        return bytes;
    }

    /**
     * Return the text file as a single String, removing all LF, CR characters,
     * and replace all multiple space characters with just 1 space.
     *
     * @param filename The text file
     * @return the text file as a single String
     */
    public static String getCleanedContents(String filename) {
        try {
            String[] lines = readLines(filename);
            StringBuilder singleLine = new StringBuilder("");
            for (String l : lines) {
                singleLine.append(l + " ");
            }
            String toReturn = singleLine.toString();
            if (toReturn.length() > 0) {
                // remove the last space character
                toReturn = toReturn.substring(0, toReturn.length() - 1);
                // remove multiple spaces, line breaks
                toReturn = toReturn.replaceAll("\\s\\s+", " ");
                toReturn = toReturn.replaceAll("\n", "");
            }
            return toReturn.trim();
        } catch (IOException e) {
            logger.error("[SOClassifierBenchmark.extractTextFile]", e);
            return null;
        }
    }

    /**
     * reads all contents from the provided file as a string (suitable for text
     * files, uses UTF-8 encoding)
     *
     * @param filename name of file to read from
     */
    public static String getContents(String filename) throws IOException {
        BufferedReader reader = getBR(filename);
        StringBuilder sbuf = new StringBuilder();
        String line = reader.readLine();
        while (line != null) {
            sbuf.append(line + "\n");
            line = reader.readLine();
        }
        reader.close();
        return sbuf.toString();
    }

    /**
     * Get the system line seperator
     */
    public static String getLineSeperator() {
        return System.getProperty("line.separator");
    }

    /**
     * get the contents of input stream as a string assuming UTF encoding.
     */
    public static String getStringContents(InputStream is) throws IOException {
        byte[] data = getByteContents(is);
        return new String(data, UTF8);
    }

    /**
     * Creates and empty file in the localDir to check if we can write to the
     * directory or not.
     */
    public static boolean isWritableDir(String localDir) {
        File f = new File(localDir);
        if (!f.exists() || !f.isDirectory()) {
            logger.error("[IOUtils.isWritableDir] not a dir " + localDir);
            return false;
        }
        return f.canWrite();
    }

    /**
     * Read an float array from a file. Each line should contain one int entry.
     * Throws parse exception is the file is malformed.
     */
    public static float[] readFloatArray(String filename) throws IOException {
        String[] lines = readLines(filename);
        float[] a = new float[lines.length];
        for (int i = lines.length - 1; i > -1; i--) {
            a[i] = Float.parseFloat(lines[i].trim());
        }
        return a;
    }

    /**
     * Read an integer array from a file. Each line should contain one int
     * entry. Throws parse exception is the file is malformed.
     */
    public static int[] readIntArray(String filename) throws IOException {
        String[] lines = readLines(filename);
        int[] a = new int[lines.length];
        for (int i = 0; i < lines.length; i++) {
            a[i] = Integer.parseInt(lines[i].trim());
        }
        return a;

    }

    /**
     * reads lines from the provided file as a list of string.
     *
     * @param filename name of file to read from
     * @return list of strings, each representing a line
     * @throws IOException in case of an error
     */
    public static String[] readLines(BufferedReader reader) throws IOException {
        List<String> lines = new ArrayList<String>();
        String line = reader.readLine();
        while (line != null) {
            lines.add(line);
            line = reader.readLine();
        }
        reader.close();
        String[] lineArray = new String[lines.size()];
        lineArray = lines.toArray(lineArray);
        return lineArray;
    }

    public static String[] readLines(String filename) throws IOException {
        return readLines(getBR(filename));
    }

    public static String[] readLines(InputStream in) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(in));
        return readLines(reader);
    }

    /**
     * Reads a String-to-String map from a file. The map is present as a set of
     * key value pairs separated by space (single pair in each line, and no
     * empty lines).
     */
    public static Map<String, String> readSSMap(String filename)
            throws IOException {
        Map<String, String> metadata = new HashMap<String, String>();
        if (!new File(filename).exists()) {
            return metadata;
        }
        String[] lines = readLines(filename);
        for (String line : lines) {
            line = line.trim();
            String[] f = line.split("\\s+");
            if (f.length != 2) {
                logger.error("[IOUtils.readSSMap] "
                        + "Invalid metadata file " + filename + " at line="
                        + line);
                throw new IOException("invalid format of the map file "
                        + filename);
            }
            metadata.put(f[0], f[1]);
        }
        return metadata;
    }

    public static String readString(InputStream io, String encoding, int max) throws IOException {
        StringWriter writer = new StringWriter();
        BufferedReader reader = new BufferedReader(new InputStreamReader(io, encoding));
        String inputLine;
        int totalLength = 0;
        while ((inputLine = reader.readLine()) != null) {
            writer.write(inputLine + "\n");
            totalLength += (inputLine.length() + 1);
            if(totalLength > max) {
            	break;
            }
        }
        writer.flush();
        io.close();
        reader.close();
        return writer.toString();
    }
    
    public static String readString(InputStream is) throws IOException {
        StringWriter writer = new StringWriter();
        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
        String inputLine;
        int totalLength = 0;
        while ((inputLine = reader.readLine()) != null) {
            writer.write(inputLine + "\n");
            totalLength += (inputLine.length() + 1);
        }
        writer.flush();
        is.close();
        reader.close();
        return writer.toString();
    }

    public static void recreateDir(String dir) {
        delete(dir);
        createDir(dir);
    }

    /**
     * Renames the source file to destination files. If the destination file
     * already exists, then it is owerwritten.
     *
     * @return true if and only if the renaming succeeded; false otherwise
     */
    public static boolean rename(String sourceFile, String destinationFile) {
        File src = new File(sourceFile);
        File dest = new File(destinationFile);
        boolean result = src.renameTo(dest);
        return result;
    }

    /**
     * Returns string which is for sure less than or equal to specified max
     * length
     */
    public static String trim(String str, int maxLen) {
        str = str.trim();
        if (str.length() > maxLen && maxLen > 3) {
            str = str.substring(0, Math.max(1, maxLen - 3)) + "...";
        }
        return str;
    }
    
    /**
     * Use for debugging print statements.
     */
    public static String info(byte[] data) {
        if (data == null) {
            return "(data=NULL)";
        }
        return "(len=" + data.length + ")";
    }

    /**
     * Use for debugging print statements.
     */
    public static String info(String s) {
        if (s == null) {
            return "(s=NULL)";
        }
        return "(len=" + s.length() + ", s=" + trim(s, 50) + ")";
    }

    /**
     * Write contents to a file. If the file already exists, its overwritten.
     */
    public static void write(byte[] contents, String filename)
            throws IOException {
        try {
            BufferedOutputStream bos = new BufferedOutputStream(
                    new FileOutputStream(filename));
            bos.write(contents);
            bos.close();
        } catch (RuntimeException e) {
            logger.error("[IOUtils.write] Error in writing " + filename
                    + " with contents " + info(contents), e);
            throw e;
        } catch (IOException e) {
            logger.error("[IOUtils.write] Error in writing " + filename
                    + " with contents " + info(contents), e);
            throw e;
        }
    }

    /**
     * Write contents to a file. If the file already exists, its overwritten.
     */
    public static void write(String contents, String filename)
            throws IOException {
    	File f = new File(filename);
        try {
        	if(!f.exists()) {
        		f.mkdirs();
        	}
            BufferedWriter bw = getBW(filename);
            bw.write(contents);
            bw.close();
        } catch (RuntimeException e) {
            logger.error("[IOUtils.write] Error in writing " + filename
                    + " with contents " + info(contents), e);
            throw e;
        } catch (IOException e) {
            logger.error("[IOUtils.write] Error in writing " + filename
                    + " with contents " + info(contents), e);
            throw e;
        } 
    }

    
    
    /**
     * Write contents of the array to a file. Each line of the file contains one
     * entry. If the file already exists, its recreated.
     */
    public static <E> void writeArray(E[] array, String filename)
            throws IOException {
        BufferedWriter bw = getBW(filename);
        for (E i : array) {
            bw.write("" + i + "\n");
        }
        bw.close();
    }

    /**
     * Write contents of the array to a file. Each line of the file contains one
     * entry. If the file already exists, its recreated.
     */
    public static void writeArray(float[] array, String filename)
            throws IOException {
        BufferedWriter bw = getBW(filename);
        for (float i : array) {
            bw.write("" + i + "\n");
        }
        bw.close();
    }

    /**
     * Write contents of the array to a file. Each line of the file contains one
     * entry. If the file already exists, its recreated.
     */
    public static void writeArray(int[] array, String filename)
            throws IOException {
        BufferedWriter bw = getBW(filename);
        for (int i : array) {
            bw.write("" + i + "\n");
        }
        bw.close();
    }

    /**
     * Writes a String-toString map to a file. The map is present as a set of
     * key value pairs separated by space (single pair in each line, and no
     * empty lines).
     */
    public static void writeSSMap(Map<String, String> metadata, String filename)
            throws IOException {
        StringBuilder sbuf = new StringBuilder();
        for (String key : metadata.keySet()) {
            sbuf.append(key + " " + metadata.get(key) + "\n");
        }
        BufferedWriter writer = getBW(filename);
        writer.write(sbuf.toString());
        writer.close();
    }

    public static boolean isFileClosed(File file) {
        try {
            Process plsof = new ProcessBuilder(new String[]{"lsof", "|", "grep", file.getAbsolutePath()}).start();
            try {
                InputStreamReader isr = new InputStreamReader(plsof.getInputStream());
                BufferedReader reader = new BufferedReader(isr);
                try {
                    String line;
                    while ((line = reader.readLine()) != null) {
                        if (line.contains(file.getAbsolutePath())) {
                            return false;
                        }
                    }
                } finally {
                    isr.close();
                    reader.close();
                }
            } finally {
                plsof.destroy();
            }
        } catch (Exception ex) {
            // TODO: handle exception ...
        }
        return true;
    }

    public static File getFileToProcess(File[] files) {
        for (int i = 0; i < files.length; i++) {
            if (IOUtils.isFileClosed(files[i])) {
                return files[i];
            } else {
                logger.info("File " + files[i].getAbsolutePath() + " is still OPEN");
            }
        }
        return null;
    }

    /**
     * See {@link #stripNonValidXMLCharacters(String)}
     */
    public static void stripNonValidXMLCharacters(Reader reader, Writer writer)
            throws IOException {
        int current = reader.read();
        while (current >= 0) {
            // here; it should not happen.
            if ((current == 0x9) || (current == 0xA) || (current == 0xD)
                    || ((current >= 0x20) && (current <= 0xD7FF))
                    || ((current >= 0xE000) && (current <= 0xFFFD))
                    || ((current >= 0x10000) && (current <= 0x10FFFF))) {
                writer.write(current);
            }
            current = reader.read();
        }
    }

    /**
     * This method ensures that the output String has only valid XML unicode
     * characters as specified by the XML 1.0 standard. For reference, please
     * see <a href="http://www.w3.org/TR/2000/REC-xml-20001006#NT-Char">the
     * standard</a>. This method will return an empty String if the input is
     * null or empty.
     * <p>
     * See http://cse-mjmcl.cse.bris.ac.uk/blog/2007/02/14/1171465494443.html
     * @param in The String whose non-valid characters we want to remove.
     * @return The in String, stripped of non-valid characters.
     */
    public static String stripNonValidXMLCharacters(String inputXML) {
        if (inputXML == null) {
            return "";
        }
        try {
            StringWriter writer = new StringWriter(inputXML.length());
            StringReader reader = new StringReader(inputXML);
            stripNonValidXMLCharacters(reader, writer);
            return writer.getBuffer().toString();
        } catch (IOException e) {
            // This should never happen
            logger.error(e);
            return "";
        }
    }

}
